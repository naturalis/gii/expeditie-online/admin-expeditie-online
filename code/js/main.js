var options = {collapsed: true, withQuotes: true, rootCollapsable: false, withLinks: true};
var json;
var source;

function toggleLogResult(ele)
{
    $('#' + $(ele).attr("data-source")).toggle();
}

function loadApiOutput(s)
{
    source = s;

    // console.log(source);

    $.ajax({
        method: "POST",
        url: "_get_api_output.php",
        data: { source: s }
    })
    .done(function( data )
    {
        console.log(data);
        $('.preview-title').html(source);
        json=$.parseJSON(data);
        $('#json-renderer').jsonViewer(json, options);
    });
}

function toggleCollapse(state)
{
    if (!state)
    {
        options.collapsed = !options.collapsed;
    }
    else
    {
        options.collapsed = state;
    }

    $('#json-renderer').jsonViewer(json, options);
    $('#preview .toggle').html(options.collapsed ? "expand all" : "collapse");
}

