<?php
		ini_set('display_errors', 0);
    include_once("lib/class.auth.php");

    $x = new Auth;
    $x->authenticate();

    $src = $_POST['source'];

    if (empty($src))
    {
        exit();
    }

    include_once("lib/class.logger.php");
    include_once("lib/class.baseClass.php");
    include_once("lib/class.api.php");

    $p = new API;

    // $p->setApiBaseUrl("https://pipeline-expeditie-online.naturalis.nl");
    $p->setApiCredentials(json_encode(["username"=>getenv("API_USER"),"password"=>getenv("API_PASS")]));
    $p->setQueryParam("limit","5");

    $paths = $p->getApiPaths();

    if (isset($paths[$src]))
    {
        $p->setAuthToken();
        $p->setUrlPath($paths[$src]);
        $p->setApiOutput();
        echo $p->getApiOutput(true);
    }
