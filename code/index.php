<?php

    ini_set('display_errors', 0);
    include_once("lib/class.auth.php");

    $x = new Auth;
    $x->authenticate();

    include_once("lib/class.logger.php");
    include_once("lib/class.baseClass.php");
    include_once("lib/class.admin.php");
    include_once("lib/class.api.php");
    include_once("lib/class.html.php");

    $a = new Admin;
    $p = new API;
    $h = new Html;

    $a->setTopstukken();
    $a->setNatuurwijzer();
    $a->setCollectors();
    $a->setCollectorSynonyms();
    $a->setNsr();
    $a->setSpecialCollections();
    $a->setWikispecies();
    $a->setTtik();
    $a->setTtikGlossary();
    $a->setXenocanto();
    $a->setNbaTaxa();
    $a->setJobLog();

    $p->setApiBaseUrl("https://pipeline-expeditie-online.naturalis.nl");
    $p->setApiCredentials(json_encode(["username"=>getenv("API_USER"),"password"=>getenv("API_PASS")]));
    $p->setAuthToken();
    $p->setUrlPath("/api/last-updated");
    $p->setApiOutput();

    $topstukken = $a->getTopstukken();
    $natuurwijzer = $a->getNatuurwijzer();
    $collectors = $a->getCollectors();
    $collectorSynonyms = $a->getCollectorSynonyms();
    $nsr = $a->getNsr();
    $specialCollections = $a->getSpecialCollections();
    $wikispecies = $a->getWikispecies();
    $ttik = $a->getTtik();
    $ttikGlossary = $a->getTtikGlossary();
    $xenocanto = $a->getXenocanto();
    $nbaTaxa = $a->getNbaTaxa();
    $jobs = $a->getJobLog();

    $lastUpdated = $p->getApiOutput()["last_updated"] ?? '';
?>

<html>
<head>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>

<script type="text/javascript" src="js/main.js"></script>

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
<link rel="stylesheet" type="text/css" href="css/main.css" />

</head>
<body>

<?php

    // echo $h->getParagaraph([
    //     '<h2 class="title">Expeditie Online Pipeline - index</h2>',
    //     '<a class="navigation" href="previews.php">previews</a>'
    // ]);

    echo $h->getParagaraph([
        '<h2 class="title">Expeditie Online Pipeline - index</h2>',
        '<a class="navigation" href="api.php">sample API output</a>'
    ]);

?>

<div class="page-frame">

    <div class="pane">

<?php


    echo $h->getTitle("Sources",3);

    echo
        $h->getParagaraph([
            "<table class='sources'>",
            $h->getTableRow(["source","#","harvest date"]),
            $h->getTableRow([
                "Topstukken",
                ["content" => number_format($topstukken["unique_articles"]),"class" => "numbers"],
                ["content" => $topstukken["last_insert"],"class" => "harvest_date","data_source" => "topstukken"]
            ]),
            $h->getTableRow([
                "Natuurwijzer",
                ["content" => number_format($natuurwijzer["unique_articles"]),"class" => "numbers"],
                ["content" => $natuurwijzer["last_insert"],"class" => "harvest_date","data_source" => "natuurwijzer"]
            ]),
            $h->getTableRow([
                "collectors",
                ["content" => number_format($collectors["unique_articles"]),"class" => "numbers"],
                ["content" => $collectors["last_insert"],"class" => "harvest_date","data_source" => "collectors"]
            ]),
            $h->getTableRow([
                "collector synonyms",
                ["content" => number_format($collectorSynonyms["unique_articles"]),"class" => "numbers"],
                ["content" => $collectorSynonyms["last_insert"],"class" => "harvest_date","data_source" => "collector_synonyms"]
            ]),
            $h->getTableRow([
                "NSR",
                ["content" => number_format($nsr["unique_articles"]),"class" => "numbers"],
                ["content" => $nsr["last_insert"],"class" => "harvest_date","data_source" => "nsr"]
            ]),
            $h->getTableRow([
                "special collections (ID's)",
                ["content" => number_format($specialCollections["unique_articles"]),"class" => "numbers"],
                ["content" => $specialCollections["last_insert"],"class" => "harvest_date","data_source" => "special_collections"]
            ]),
            $h->getTableRow([
                "NBA taxa",
                ["content" => number_format($nbaTaxa["unique_articles"]),"class" => "numbers"],
                ["content" => $nbaTaxa["last_insert"],"class" => "harvest_date","data_source" => "nba_taxa"]
            ]),
            $h->getTableRow([
                "Wikidata",
                ["content" => number_format($wikispecies["unique_articles"]),"class" => "numbers"],
                ["content" => $wikispecies["last_insert"],"class" => "harvest_date","data_source" => "wikispecies"]
            ]),
            $h->getTableRow([
                "TTIK (taxa)",
                ["content" => number_format($ttik["unique_articles"]),"class" => "numbers"],
                ["content" => $ttik["last_insert"],"class" => "harvest_date","data_source" => "ttik"]
            ]),
            $h->getTableRow([
                "TTIK (lemmas)",
                ["content" => number_format($ttikGlossary["unique_articles"]),"class" => "numbers"],
                ["content" => $ttikGlossary["last_insert"],"class" => "harvest_date","data_source" => "ttik_glossary"]
            ]),
            $h->getTableRow([
                "Xeno-canto",
                ["content" => number_format($xenocanto["unique_articles"]),"class" => "numbers"],
                ["content" => $xenocanto["last_insert"],"class" => "harvest_date","data_source" => "xenocanto"]
            ]),
            "</table>"
        ]);
?>

</div>
<div class="pane">

<?php


    $b=[];
    $b[]=$h->getTitle("Natuurwijzer items by subject",3);
    $b[]="<table class='sources'>";
    $b[]=$h->getTableRow(["tag","#"]);
    foreach($natuurwijzer["data"] as $key => $val)
    {
        $b[] = $h->getTableRow([$key,number_format(count($val))]);
    }
    $b[]="</table>";

    echo $h->getParagaraph($b);

?>

</div>
<div class="pane">

<?php

    $b=[];
    $b[]=$h->getTitle("TTIK articles by category",3);
    $b[]="<table class='sources'>";
    $b[]=$h->getTableRow(["tag","#"]);
    foreach($ttikGlossary["data"] as $key => $val)
    {
        $b[] = $h->getTableRow([$key,number_format(count($val))]);
    }
    $b[]="</table>";

    echo $h->getParagaraph($b);

?>

    </div>

</div>

<div class="page-frame">

    <div class="pane">
<?php

    $b=[];
    $b[]=$h->getTitle("Last updated (API)",3);
    $b[] = $lastUpdated;
    echo $h->getParagaraph($b);
?>
    </div>

    <div class="pane">
<?php


    $b=[];
    $b[]=$h->getTitle("Job log (last 50)",3);
    $b[]="<table class='log'>";
    $b[]=$h->getTableRow(["type","name","start","finish","&#128270;"]);
    foreach($jobs as $key => $val)
    {
        $b[] = $h->getTableRow([
            $val["job_type"],
            $val["job_name"],
            $val["job_started"],
            $val["job_finished"],
            [
                "class" => "clickable",
                "data-source" => "job-" . $val["id"],
                "onclick" => "toggleLogResult(this);",
                "title" => "click to toggle details",
                "content" => "&#128270;"
            ]
        ]);

        $b[] = $h->getTableRow([
            [
                "class" => "job-result hidden",
                "id" => "job-" . $val["id"],
                "content" =>
                    preg_replace(["/Array\n/","/^\(\n/","/\n\)$/"], "",
                        print_r(
                            json_decode($val["job_result"],true),
                            true)
                    ) . "<br />",
                "colspan" => 5
            ]
        ]);

    }
    $b[]="</table>";

    echo $h->getParagaraph($b);

?>
    </div>

</div>

</body>
</html>
