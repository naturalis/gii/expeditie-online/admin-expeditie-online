<?php

class Html
{

    public function getTableRow($cells)
    {
        $buffer = [];
        $cells = is_array($cells) ? $cells  : [ $cells ];

        foreach($cells as $cell)
        {
            if (is_array($cell))
            {
                $buffer[] = '<td
                    class="' . $cell["class"] . '"
                    title="' . ($cell["title"] ?? "") . '"
                    colspan="' . ($cell["colspan"] ?? "1") . '"
                    onclick="' . ($cell["onclick"] ?? "") . '"
                    id="' . ($cell["id"] ?? "") . '"
                    data-source="'. ($cell["data-source"] ?? "") . '">' . $cell["content"] . "</td>";
            }
            else
            {
                $buffer[] = "<td>$cell</td>";
            }
        }
        return "<tr>" . implode("",$buffer) . "</tr>" . "\n";
    }

    public function getParagaraph($elements)
    {
        $elements = is_array($elements) ? $elements  : [ $elements ];
        return "\n<p>\n" . implode("",$elements) . "\n</p>\n";
    }

    public function getTitle($text,$size=1)
    {
        return "<h".$size.">$text</h".$size.">";
    }
}
