<?php

class Admin extends BaseClass
{
    private $topstukken;
    private $natuurwijzer;
    private $collectors;
    private $collector_synonyms;
    private $nsr;
    private $nba_taxa;
    private $job_log;
    private $special_collections;
    private $wikispecies;
    private $ttik;
    private $ttik_glossary;
    private $xenocanto;
    private $jobs;
    protected $valid_general_types = [
        "collector",
        "highlight",
        "special",
        "subcollection",
        "special-collection",
        "type-status",
        "taxon"
    ];

    public function __construct ()
    {
        parent::__construct();
    }

    public function setSampleSize($sample_size)
    {
        $this->sample_size = $sample_size;
    }

    public function setTopstukken()
    {
        $this->topstukken = $this->getGeneric("topstukken");
        // print_r($this->topstukken);
    }

    public function setNatuurwijzer()
    {
        $this->natuurwijzer["unique_articles"]=0;

        $stmt = $this->db->prepare("select * from natuurwijzer order by inserted desc");
        $result = $stmt->execute();

        $i=0;

        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            !empty($this->natuurwijzer["last_insert"]) || $this->natuurwijzer["last_insert"] = $res["inserted"];

            if (!empty($res["collections"]))
            {
                foreach(json_decode($res["collections"],true) as $tag)
                {
                    $this->natuurwijzer["data"]["collections"][$tag][] = $res["id"];
                }
            }
            if (!empty($res["taxon"]))
            {
                foreach(json_decode($res["taxon"],true) as $tag)
                {
                    $this->natuurwijzer["data"]["taxa"][$tag][] = $res["id"];
                }
            }

            (empty($res["collections"]) && empty($res["taxon"])) || $this->natuurwijzer["unique_articles"]++;
        }

        // print_r($this->natuurwijzer);
    }

    public function setCollectors()
    {
        $stmt = $this->db->prepare("select count(distinct collector) as unique_articles, max(inserted) as last_insert from collector_synonyms");
        $result = $stmt->execute();
        $res = $result->fetchArray(SQLITE3_ASSOC);
        $this->collectors["last_insert"] = $res["last_insert"];
        $this->collectors["unique_articles"] = $res["unique_articles"];
    }

    public function setCollectorSynonyms()
    {
        $this->collector_synonyms = $this->getGeneric("collector_synonyms");
        // print_r($this->collector_synonyms);
    }

    public function setNsr()
    {
        $this->nsr = $this->getGeneric("nsr");
        // print_r($this->nsr);
    }

    public function setSpecialCollections()
    {
        $this->special_collections = $this->getGeneric("special_collections_ids");
        // print_r($this->special_collections);
    }

    public function setWikispecies()
    {
        $this->wikispecies = $this->getGeneric("wikispecies");
        // print_r($this->wikispecies);
    }

    public function setTtik()
    {
        $stmt = $this->db->prepare("select count(*) as unique_articles from taxa");
        $result = $stmt->execute();
        $res = $result->fetchArray(SQLITE3_ASSOC);
        $this->ttik["unique_articles"] = $res["unique_articles"];

        $stmt = $this->db->prepare("select job_finished from job_log where job_name = 'TTIK (taxa)' order by id desc limit 1");
        $result = $stmt->execute();
        $res = $result->fetchArray(SQLITE3_ASSOC);
        $this->ttik["last_insert"] = $res["job_finished"];
    }

    public function setTtikGlossary()
    {
        $stmt = $this->db->prepare("select synonym from glossary_synonyms where language_id = $this->ttik_language_id");
        $result = $stmt->execute();
        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            $t = explode("|",$res["synonym"]);
            $this->ttik_glossary["data"][$t[0]][] = $t[1];
        }

        $stmt = $this->db->prepare("select count(distinct glossary_id) as unique_articles from glossary_synonyms where language_id = $this->ttik_language_id");
        $result = $stmt->execute();
        $res = $result->fetchArray(SQLITE3_ASSOC);
        $this->ttik_glossary["unique_articles"] = $res["unique_articles"];

        $stmt = $this->db->prepare("select job_finished from job_log where job_name = 'TTIK (glossary)' order by id desc limit 1");
        $result = $stmt->execute();
        $res = $result->fetchArray(SQLITE3_ASSOC);
        $this->ttik_glossary["last_insert"] = $res["job_finished"];
    }

    public function setXenocanto()
    {
        $this->xenocanto = $this->getGeneric("xenocanto");
        // print_r($this->xenocanto);
    }

    public function setNbaTaxa()
    {
        $this->nba_taxa = $this->getGeneric("nba_taxa");
    }

    public function setJobLog()
    {
        $stmt = $this->db->prepare("select * from job_log order by job_started desc limit 50");
        $result = $stmt->execute();
        while ($res = $result->fetchArray(SQLITE3_ASSOC))
        {
            $this->job_log[] = $res;
        }
    }

    public function getTopstukken()
    {
        return $this->topstukken;
    }

    public function getNatuurwijzer()
    {
        return $this->natuurwijzer;
    }

    public function getCollectors()
    {
        return $this->collectors;
    }

    public function getCollectorSynonyms()
    {
        return $this->collector_synonyms;
    }

    public function getNsr()
    {
        return $this->nsr;
    }

    public function getSpecialCollections()
    {
        return $this->special_collections;
    }

    public function getWikispecies()
    {
        return $this->wikispecies;
    }

    public function getTtik()
    {
        return $this->ttik;
    }

    public function getTtikGlossary()
    {
        return $this->ttik_glossary;
    }

    public function getXenocanto()
    {
        return $this->xenocanto;
    }

    public function getNbaTaxa()
    {
        return $this->nba_taxa;
    }

    public function getJobLog()
    {
        return $this->job_log;
    }
}
