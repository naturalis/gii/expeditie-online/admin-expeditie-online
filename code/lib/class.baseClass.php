<?php

class BaseClass
{
    protected $db;
    protected $now;
    protected $ttik_language_id;

    public function __construct()
    {
        $this->logger = new Logger();

        $this->db_path = getenv('DATABASE_PATH');

        if (empty($this->db_path))
        {
            throw new Exception("empty database path", 1);

        }

        $this->db = new SQLite3($this->db_path);

        if (isset($this->table_def))
        {
            if (is_array($this->table_def))
            {
                foreach ($this->table_def as $val)
                {
                    $this->db->exec($val);
                }
            }
            else
            {
                $this->db->exec($this->table_def);
            }
        }

        $this->now = date("c");
        $this->ttik_language_id = 24;
    }

    public function __destruct()
    {
        if ($this->db)
        {
            $this->db->close();
        }
    }

    protected function getGeneric($table)
    {
        $stmt = $this->db->prepare("select count(*) as unique_articles, max(inserted) as last_insert from $table");
        $result = $stmt->execute();
        return $result->fetchArray(SQLITE3_ASSOC);
    }

}