<?php

class Auth
{
    private $valid_username=false;
    private $valid_password=false;
    private $user_username;
    private $user_password;

    public function __construct ()
    {
        $this->setUserCredentials();
        $this->setValidCredentials();
    }

    final function authenticate()
    {
        if (
            (!isset($this->valid_username) || !isset($this->valid_password)) ||
            (!isset($this->user_username) || !isset($this->user_password)) ||
            (($this->user_username != $this->valid_username) || $this->user_password != $this->valid_password)
        )
        {
            header('WWW-Authenticate: Basic realm="Expeditie Online"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'unauthorized';
            exit;
        }
    }

    private function setUserCredentials()
    {
        $this->user_username = ($_SERVER['PHP_AUTH_USER'] ?? null);
        $this->user_password = ($_SERVER['PHP_AUTH_PW'] ?? null);
    }

    private function setValidCredentials()
    {
        $this->valid_username = getenv("PIPELINE_AUTH_USER");
        $this->valid_password = getenv("PIPELINE_AUTH_PASS");
    }

}