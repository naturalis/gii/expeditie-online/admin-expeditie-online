<?php

class API extends BaseClass
{
    private $api_credentials=[];
    private $api_output;
    private $curl;
    private $url;
    private $curl_error;
    private $curl_error_msg;
    private $curl_info;
    private $curl_response;
    private $token;
    private $last_updated;
    private $api_base_url = 'https://pipeline-expeditie-online.naturalis.nl';
    private $url_path;
    private $query_params=[];

    private $api_paths = [
        "highlights" => "/api/highlight",
        "taxa" => "/api/taxon",
        "preparation types" => "/api/preparation-type",
        "type statuses" => "/api/type-status",
        "subcollections" => "/api/subcollection",
        "collectors" => "/api/collector",
        "special collections" => "/api/special-collection",
        "names" => "/api/names"
    ];

    public function __construct ()
    {
        parent::__construct();
        $this->curl = curl_init();
    }

    public function getApiPaths()
    {
        return $this->api_paths;
    }

    public function setApiBaseUrl($api_base_url)
    {
        $this->api_base_url = $api_base_url;
    }

    public function setUrlPath($url_path)
    {
        $this->url_path = $url_path;
    }

    public function setQueryParam($key,$val)
    {
        $this->query_params[] = [ "key" => $key, "val" => $val ];
    }

    public function resetQueryParams()
    {
        $this->query_params = [];
    }

    public function setApiCredentials($api_credentials)
    {
        $this->api_credentials = $api_credentials;
    }

    public function setAuthToken()
    {
        $this->url = $this->api_base_url . "/auth";
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $this->api_credentials);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
        curl_setopt($this->curl, CURLOPT_POST, true);
        $this->curl_get_page();
        $this->token = json_decode($this->curl_response)->access_token ?? '';
    }

    public function setApiOutput()
    {
        $q = implode("&", array_map(function($a) { return $a["key"]."=". urlencode($a["val"]); },$this->query_params));
        $this->url = $this->api_base_url . $this->url_path . ( $q ? "?" . $q : "" );
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, ["Authorization: JWT $this->token"]);
        curl_setopt($this->curl, CURLOPT_POST, false);
        $this->curl_get_page();
        $this->api_output = $this->curl_response;
    }

    public function getApiOutput($raw=false)
    {
        if ($raw)
        {
            return $this->api_output;
        }
        else
        {
            return json_decode($this->api_output,true);
        }
    }

    private function curl_get_page()
    {
        curl_setopt($this->curl, CURLOPT_URL,$this->url);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);

        $this->curl_response = curl_exec( $this->curl );

        $this->curl_error=curl_errno( $this->curl );
        $this->curl_error_msg=curl_error( $this->curl );
        $this->curl_info=curl_getinfo( $this->curl );

        if (!empty($this->curl_error))
        {
            throw new Exception( $this->curl_error_msg );
        }
    }
}
