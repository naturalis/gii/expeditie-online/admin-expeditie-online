<?php

class Logger
{

    private $calling_class_override;
    private $logfile;

    public function setCallingClassOverride($calling_class_override)
    {
        $this->calling_class_override = $calling_class_override;
    }

    public function setFile($logfile)
    {
        if (!file_exists($logfile))
        {
            $this->log("logfile $logfile doesn't exist" ,1);
            return;
        }

        if (!is_writable($logfile))
        {
            $this->log("logfile $logfile isn't writeable" ,1);
            return;
        }

        $this->logfile = $logfile;
    }

    public function log($message, $level=3)
    {
        $levels = [
            1 => 'Error',
            2 => 'Warning',
            3 => 'Info',
            4 => 'Debug',
        ];
        $logline = date('c') . ' - ' . ($this->calling_class_override ?? $this->getCallingClass()) . ' - ' .
            $levels[$level] . ' - ' . $message . "\n";

        echo $logline;

        if ($this->logfile)
        {
            file_put_contents($this->logfile, $logline, FILE_APPEND | LOCK_EX);
        }
    }

    private function getCallingClass ()
    {
        $trace = debug_backtrace();
        $class = $trace[1]['class'];
        for ($i = 1; $i < count($trace); $i++) {
            if (isset($trace[$i])) {
                if ($class != $trace[$i]['class']) {
                    return $trace[$i]['class'];
                }
            }
        }
    }
}