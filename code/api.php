<?php

    ini_set('display_errors', 0);
    include_once("lib/class.auth.php");

    $x = new Auth;
    $x->authenticate();

    include_once("lib/class.logger.php");
    include_once("lib/class.baseClass.php");
    include_once("lib/class.admin.php");
    include_once("lib/class.api.php");
    include_once("lib/class.html.php");

    $a = new Admin;
    $p = new API;
    $h = new Html;

?>
<html>
<head>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>

<script type="text/javascript" src="js/main.js"></script>

<link rel="stylesheet" type="text/css" href="css/jquery.json-viewer.css" media="screen">
<script type="text/javascript" src="js/jquery.json-viewer.js"></script>
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />

<link rel="stylesheet" type="text/css" href="css/main.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.json-viewer.css" />

</head>
<script>
$( document ).ready(function()
{
    $('.api-link').on('click',function()
    {
        loadApiOutput($(this).attr('data-source'));
    });
});
</script>

<body id="previews">


<?php

    echo $h->getParagaraph([
        '<h2 class="title">Expeditie Online Pipeline - sample API output</h2>',
        '<a class="navigation" href="index.php">index</a>'
    ]);

?>

    <div class="page-frame">

        <div class="pane">
<?php

    foreach ($p->getApiPaths() as $key => $url)
    {
        echo $h->getTitle('<span class="api-link" data-source="' . $key . '">' . $key . '</span>',4);

    }

?>
        </div>

    </div>
    <div class="page-frame">

        <div class="pane api-output">
            <div class="preview-title">(endpoint)</div>
            <div class="clickable preview_toggle" onclick="toggleCollapse()">toggle expanded JSON</div>
            <pre id="json-renderer">(output)</pre>
        </div>

    </div>

</body>
</html>
